#include <TheThingsNetwork.h>
#include <SoftwareSerial.h>

// Set your AppEUI and AppKey 2017
const byte appEui[8] = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x00, 0x0F, 0x56 }; //for example: {0x70, 0xB3, 0xD5, 0x7E, 0xE0, 0xE0, 0x01, 0x4A1};
const byte appKey[16] = { 0xA9, 0x16, 0x79, 0x6B, 0x31, 0xBD, 0x08, 0x39, 0x6C, 0xC6, 0x4C, 0xF1, 0xE3, 0x44, 0x86, 0x80 }; //for example: {0x73, 0x6D, 0x24, 0xD2, 0x69, 0xBE, 0xE3, 0xAE, 0x0E, 0xCE, 0xF0, 0xBB, 0x6C, 0xA4, 0xBA, 0xFE};

SoftwareSerial loraSerial(10,9);
#define debugSerial Serial

#define debugPrintLn(...) { if (debugSerial) debugSerial.println(__VA_ARGS__); }
#define debugPrint(...) { if (debugSerial) debugSerial.print(__VA_ARGS__); }

#define LEDPIN 8
#define Reset  15

TheThingsNetwork ttn;

byte buf[2];

void setup()
{
  buf[0] = 00;
  buf[1] = 00;
  
  // Setup outputs
  pinMode(LEDPIN, OUTPUT);
  pinMode(Reset, OUTPUT);
  
  digitalWrite(Reset, LOW);
  delay(500);
  digitalWrite(Reset, HIGH);
  delay(1000); // give it a little time
  
  debugSerial.begin(115200);
  loraSerial.begin(57600);

  delay(1000);
  ttn.init(loraSerial, debugSerial);
  ttn.reset();
  
  if (!ttn.join(appEui, appKey)) {
    delay(6000);
  }

  delay(6000);
  ttn.showStatus();
  debugPrintLn("Setup for The Things Network complete");

  delay(1000);
}

void loop() {
  // Send a byte
  int downlinkBytes = ttn.sendBytes(buf, 2);

  if (downlinkBytes > 0) {
    
    debugPrintLn("Received " + String(downlinkBytes) + " bytes")
    
    // Print the received bytes
    
    for (int i = 0; i < downlinkBytes; i++) {  
      if (i < 2) {
        buf[i] = ttn.downlink[i];
      }
      
      debugPrint(String(ttn.downlink[i]) + " ");
    }
    
    debugPrintLn("Waiting...");
  }

  delay(20000);
}
