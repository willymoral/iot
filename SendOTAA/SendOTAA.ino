#include <TheThingsNetwork.h>
#include <SoftwareSerial.h>   // 
#include <DHT.h>              //

// Set your AppEUI and AppKey
const byte appEui[8] = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x00, 0x0F, 0x56 };
const byte appKey[16] = { 0x80, 0x86, 0x44, 0xE3, 0xF1, 0x4C, 0xC6, 0x6C, 0x39, 0x08, 0xBD, 0x31, 0x6B, 0x79, 0x16, 0xA9 };

SoftwareSerial loraSerial(10,9);
#define debugSerial Serial

// Set your message to send
String message = "Hello world"; //sending a string of chars "Hello world"

#define debugPrintLn(...) { if (debugSerial) debugSerial.println(__VA_ARGS__); }
#define debugPrint(...) { if (debugSerial) debugSerial.print(__VA_ARGS__); }

#define DHTPIN 6
#define LEDPIN 8
#define Reset  15

#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

TheThingsNetwork ttn;

void setup() {
   // Setup outputs
  pinMode(LEDPIN, OUTPUT);
  pinMode(Reset, OUTPUT);
  
  digitalWrite(Reset, LOW);
  delay(500);
  digitalWrite(Reset, HIGH);
  delay(1000); // give it a little time

  // Initialize DHT11
  dht.begin();
  
  loraSerial.begin(57600);
  debugSerial.begin(9600);
    
  // Wait a maximum of 10s for Serial Monitor
  while (!debugSerial && millis() < 10000);

  ttn.init(loraSerial, debugSerial);
  ttn.reset();
  
  // OTA
   while(!ttn.join(appEui, appKey)){
      delay(6000);
  }

  debugSerial.println("-- STATUS OTAA");
  ttn.showStatus();
}

void loop() {
  debugSerial.println("-- LOOP");
  
  debugSerial.println(">>> Read Humidity");

  // Read sensor values and multiply by 100 to effictively have 2 decimals
  uint16_t humidity = dht.readHumidity(false) * 100;

  debugSerial.println(">>> Read Temperature");
  // false: Celsius (default)
  // true: Farenheit
  uint16_t temperature = dht.readTemperature(false) * 100;

  // Split both words (16 bits) into 2 bytes of 8
  byte payload[2];
  payload[0] = highByte(temperature);
  payload[1] = lowByte(temperature);

  //payload[2] = highByte(humidity);
  //payload[3] = lowByte(humidity);
  
  debugSerial.print("Celsius: ");
  debugSerial.println(temperature);

  debugSerial.println(">>> Send Data");
  
  digitalWrite(LEDPIN, HIGH);
  ttn.sendBytes(payload, sizeof(payload));
  digitalWrite(LEDPIN, LOW);
  
  delay(10000);
}
