#include <TheThingsNetwork.h> //
#include <SoftwareSerial.h>   // 
#include <DHT.h>              //

// Set your AppEUI and AppKey
const byte devAddr[4] = { 0x2F, 0x82, 0xF8, 0x11 };

// Set your Network Session Key, for example: { 0x2B, 0x7E, 0x15, 0x16, ... };
// This is used by the network to identify your device
const byte nwkSKey[16] = { 0xF0, 0x7B, 0x63, 0x77, 0xEF, 0x35, 0x32, 0x97, 0x90, 0xB2, 0x18, 0x2D, 0x39, 0xBE, 0xCD, 0x7F };

// Set your Application Session Key, for example: { 0x2B, 0x7E, 0x15, 0x16, ... };
// This is used by the network for encryption
const byte appSKey[16] = { 0xAC, 0xB4, 0x57, 0x3B, 0x47, 0x01, 0x1A, 0x8F, 0x4D, 0x56, 0x43, 0x75, 0xF7, 0x50, 0xEF, 0xA3 };

// OTA
const byte appEui[8] = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x00, 0x0F, 0x56 };
const byte appKey[16] = { 0xA9, 0x16, 0x79, 0x6B, 0x31, 0xBD, 0x08, 0x39, 0x6C, 0xC6, 0x4C, 0xF1, 0xE3, 0x44, 0x86, 0x80 };

SoftwareSerial loraSerial(10,9);

#define debugSerial Serial

#define DHTTYPE DHT11
#define DHTPIN 6
#define Reset  15

DHT dht(DHTPIN, DHTTYPE);

TheThingsNetwork ttn;

void setup() {
  
  // Setup outputs
  pinMode(Reset, OUTPUT);
  
  digitalWrite(Reset, LOW);
  delay(500);
  digitalWrite(Reset, HIGH);
  delay(1000); // give it a little time

  // Initialize DHT11
  dht.begin();
  
  loraSerial.begin(57600);
  debugSerial.begin(9600);
    
  // Wait a maximum of 10s for Serial Monitor
  while (!debugSerial && millis() < 10000);

  ttn.init(loraSerial, debugSerial);
  ttn.reset();
  
  // ABP
  //ttn.personalize(devAddr, nwkSKey, appSKey);

  // OTA
   while(!ttn.join(appEui, appKey)){
      delay(6000);
   }

  debugSerial.println("-- STATUS");
  ttn.showStatus();
  
  delay(300);

  Serial.println("adr off: " + ttn.sendRawCommand("mac set adr off"));
  Serial.println("ar off: " + ttn.sendRawCommand("mac set ar off"));
  Serial.println("pwridx off: " + ttn.sendRawCommand("mac set pwridx 1"));
  Serial.println("dr off: " + ttn.sendRawCommand("mac set dr 0"));
  Serial.println("save: " + ttn.sendRawCommand("mac save"));
  
  Serial.println("Ver : " + ttn.sendRawCommand("sys get ver"));
  
  delay(300);
}

void loop() {  
  
  //Serial.println("Ver      : " + ttn.sendRawCommand("sys get ver"));
  //Serial.println("RX2      : " + ttn.sendRawCommand("mac set rx2 10 923300000"));
  //Serial.println("Retx     : " + ttn.sendRawCommand("mac set retx 5"));
  //Serial.println("Retx     : " + ttn.sendRawCommand("mac set ch drrange 10 0 2"));

  
  
  
  //Serial.println("Mod      : " + ttn.sendRawCommand("radio get mod"));
  //Serial.println("Freq     : " + ttn.sendRawCommand("radio get freq"));
  //Serial.println("SF       : " + ttn.sendRawCommand("radio get sf"));
  //Serial.println("BW       : " + ttn.sendRawCommand("radio get bw"));
  //Serial.println("CR       : " + ttn.sendRawCommand("radio get cr"));
  //Serial.println("PRLEN    : " + ttn.sendRawCommand("radio get prlen"));
  //Serial.println("PWR      : " + ttn.sendRawCommand("radio get pwr"));
  //Serial.println("Rxdelay1 : " + ttn.sendRawCommand("mac get rxdelay1"));
  //Serial.println("Rx2      : " + ttn.sendRawCommand("mac get rx2"));
  
  //debugSerial.println("-- LOOP");
  //debugSerial.println(">>> Read Humidity");
  
  // Read sensor values and multiply by 100 to effictively have 2 decimals
  uint16_t humidity = dht.readHumidity(false) * 100;

  //debugSerial.println(">>> Read Temperature");
  // false: Celsius (default)
  // true: Farenheit
  uint16_t temperature = dht.readTemperature(false) * 100;

  // Split both words (16 bits) into 2 bytes of 8
  byte payload[2];
  payload[0] = highByte(temperature);
  payload[1] = lowByte(temperature);

  //payload[2] = highByte(humidity);
  //payload[3] = lowByte(humidity);
  
  debugSerial.print("Celsius: ");
  debugSerial.println(temperature);

  debugSerial.print("Humidity: ");
  debugSerial.println(humidity);
  
  debugSerial.println(">>> Send Data");
  
  ttn.sendBytes(payload, sizeof(payload));

  delay(15000);
}
